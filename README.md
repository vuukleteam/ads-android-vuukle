**Implement Vuukle Ads SDK for Android** Click for download apk: [VuukleAdsSample apk file for demo](app/docs/app-debug.apk) 


1. Add the JitPack repository to your build file
   ```kotlin
   allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
        }
   }
   ```
   
   
2. Include Vuukle Ads SDK dependency in **build.gradle**

   ```kotlin
   // Load Vuukle ads sdk
   implementation 'com.github.vuukle:ads-android-vuukle:1.0.7'
   ```

3. Reach out to vuukle team on **ad@vuukle.com** to get your Application ID.

```xml
<manifest>
    <application>
        <meta-data
            android:name="com.google.android.gms.ads.APPLICATION_ID"
            android:value="ca-app-pub-7538703090817389~**********"/>
    </application>
	</manifest>
```

4. Add permission in **AndroidManifest.xml**

```xml
<uses-permission android:name="android.permission.INTERNET" />
```

5. Define **VuukleAdView** in layout XML.

```xml
   <?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:ads="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

   <vuukle.sdk.ads.widget.VuukleAdView
           android:id="@+id/vuukle_ad_view_top"
           android:layout_width="300dp"
           android:layout_height="250dp"
           android:layout_marginTop="8dp"
           app:layout_constraintEnd_toEndOf="parent"
           app:layout_constraintStart_toStartOf="parent"
           app:layout_constraintTop_toTopOf="parent" />

   <vuukle.sdk.ads.widget.VuukleAdView
           android:id="@+id/vuukle_ad_view_bottom"
           android:layout_width="300dp"
           android:layout_height="250dp"
           android:layout_marginBottom="8dp"
           app:layout_constraintBottom_toBottomOf="parent"
           app:layout_constraintEnd_toEndOf="parent"
           app:layout_constraintStart_toStartOf="parent"
           app:layout_constraintTop_toBottomOf="@+id/vuukle_ad_view_top"
           app:layout_constraintVertical_bias="1.0" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

6. Call **initAds** function in activity **onCreate()**. Should provide the App id and specify the corresponding UnitId and AdSize programmatically.

```kotlin
private fun initAds() {
        // Initialize Vuukle Ads
        vuukleAds.initialize(this)
        // Create Top banners
        vuukleAdViewTop.setAdSize(VuukleAdSize.Type.BANNER)
        vuukleAdViewTop.setAdUnitId(AdsConstants.HomePage.BANNER_AD_UNIT_ID_1)
        vuukleAds.createBanner(vuukleAdViewTop)
        // Create Bottom banners
        vuukleAdViewBottom.setAdSize(VuukleAdSize.Type.BANNER)
        vuukleAdViewBottom.setAdUnitId(AdsConstants.HomePage.BANNER_AD_UNIT_ID_2)
        vuukleAds.createBanner(vuukleAdViewBottom)
        // Observing Ads results
        vuukleAds.addResultListener(object : VuukleAdsResultCallback {
            override fun onDemandFetched(id: String) {
                Toast.makeText(this@BannerActivity, id, Toast.LENGTH_SHORT).show()
            }
        })
        // Handling errors
        vuukleAds.addErrorListener(object : VuukleAdsErrorCallback {
            override fun onError(error: VuukleAdsException) {
                Log.i("ewfwefwe--->>", error.toString())
            }
        })
        // start advertisement
        vuukleAds.startAdvertisement()
    }
```